# Build the image
docker-compose up -d

# Fail if the build update has failed
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo "build failed"
    exit $RESULT
fi

# Tag and push the image
CONTAINER_NAME=empdir-db_db_1
sleep 10
docker exec -it $CONTAINER_NAME su postgres -c '/usr/local/bin/pg_ctl stop'
sleep 10
docker commit -m "Tagging the container for push" $CONTAINER_NAME devopsplayground/empdir-db:latest
docker push devopsplayground/empdir-db:latest

# Cleanup
docker-compose down --rmi local