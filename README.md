This repository holds code sample for a database update mechanism using mechanism using gradle as well as maven

## Gradle
You need to have gradle installed (or can use the gradle wrapper that comes with the repo) to run the update using one of the following commands

* `gradle update`
* `gradlew update`

## Maven
You need to have maven installed to run the update using the following command

* `mvn resources:resources liquibase:update`
